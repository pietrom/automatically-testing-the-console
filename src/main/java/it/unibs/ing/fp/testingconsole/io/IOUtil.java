package it.unibs.ing.fp.testingconsole.io;

import java.io.PrintStream;
import java.util.Scanner;

public class IOUtil {
	private final Scanner in;
	private final PrintStream out;
	
	public IOUtil(Scanner scanner, PrintStream out) {
		this.in = scanner;
		this.out = out;
	}

	public String readString(String message) {
		out.print(message);
		return in.next();
	}

	public void println(String message) {
		out.println(message);
	}

	public boolean readBoolean(String message, String yes, String no) {
		Boolean result = null;
		while(result == null) {
			out.print(message);
			String input = in.next();
			if(yes.equalsIgnoreCase(input)) {
				result = Boolean.TRUE;
			} else if(no.equalsIgnoreCase(input)) {
				result = Boolean.FALSE;
			}
		}
		out.println();
		return result;
	}
}
