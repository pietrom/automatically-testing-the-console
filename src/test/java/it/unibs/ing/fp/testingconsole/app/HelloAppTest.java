package it.unibs.ing.fp.testingconsole.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Test;

public class HelloAppTest {
	
	@Test
	public void singleExecutionAndExit() throws Exception {
		final String newLine = System.getProperty("line.separator");
		
		final Scanner scanner = new Scanner("Duke n");
		scanner.useDelimiter(" ");
		
		ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(outputBuffer);
		
		final HelloApp app = new HelloApp(scanner, out);
		app.run();
		final String output = outputBuffer.toString();
		assertTrue(output.startsWith("Welcome to HelloApp!"));
		assertTrue(output.endsWith("GoodBye" + newLine));
		assertTrue(output.contains("Message is: Hello, Duke!"));
		String[] items = output.split("Message destination\\?");
		assertEquals(2, items.length);
	}
	
	@Test
	public void tripleExecutionAndExit() throws Exception {
		final String newLine = System.getProperty("line.separator");
		
		final Scanner scanner = new Scanner("Duke y Goofy y Donald n");
		scanner.useDelimiter(" ");
		
		ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(outputBuffer);
		
		final HelloApp app = new HelloApp(scanner, out);
		app.run();
		final String output = outputBuffer.toString();
		assertTrue(output.startsWith("Welcome to HelloApp!"));
		assertTrue(output.endsWith("GoodBye" + newLine));
		assertTrue(output.contains("Message is: Hello, Duke!"));
		assertTrue(output.contains("Message is: Hello, Goofy!"));
		assertTrue(output.contains("Message is: Hello, Donald!"));
		String[] items = output.split("Message destination\\?");
		assertEquals(4, items.length);
	}
	
	
}
